#!/usr/bin/bash

mkdir -p `dirname $0`/build
cd `dirname $0`/build
cmake -DCMAKE_PREFIX_PATH=`dirname $0`/../install -DCMAKE_INSTALL_PREFIX=../install ../
make -j16
make install
cd -
