#pragma once

#include "FastFrames/MainFrame.h"

#include "FastFrames/ConfigSetting.h"
#include "FastFrames/UniqueSampleID.h"

#include "Math/Vector4D.h"
#include "ROOT/RDataFrame.hxx"
#include "TClass.h"

#include <memory>

#include <onnxruntime_cxx_api.h>
#include <array>

using TLV = ROOT::Math::PtEtaPhiEVector;

class UniqueSampleID;

class SAJAEvaluator : public MainFrame {
public:

  explicit SAJAEvaluator() = default;

  virtual ~SAJAEvaluator() = default;

  virtual void init() override final;

  virtual ROOT::RDF::RNode defineVariables(ROOT::RDF::RNode mainNode,
                                           const UniqueSampleID& id) override final;
  
  virtual ROOT::RDF::RNode defineVariablesNtuple(ROOT::RDF::RNode mainNode,
                                                 const UniqueSampleID& id) override final;

  virtual ROOT::RDF::RNode defineVariablesTruth(ROOT::RDF::RNode node,
                                                const std::string& truth,
                                                const UniqueSampleID& sampleID) override final;
private:

  ClassDefOverride(SAJAEvaluator, 1);
  Ort::Session *m_session;

  float evaluate(const ROOT::RVec<ROOT::Math::PtEtaPhiEVector>& jet, 
                 const ROOT::RVec<double>& jet_btagScore, 
                 const ROOT::RVec<float>& jet_ghostTrackPtWeightedCharge, 
                 const ROOT::RVec<std::vector<float>>& jet_ePerSampling, 
                 const ROOT::RVec<ROOT::Math::PtEtaPhiEVector>& el_TLV, 
                 const ROOT::RVec<ROOT::Math::PtEtaPhiEVector>& mu_TLV, 
                 const float& met_met, const float& met_phi, 
                 const char& pass_emu_tight, const char& pass_ee_tight, const char& pass_mumu_tight);
};
