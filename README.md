# CustomFrame for inference of the SAJA dilepton ONNX model
Self-Attention for Jet Assignment (SAJA) dilepton model is used to search for $t \to W^{+}s$ decay.

Adapted from the original SAJA model [[1]](#1) used for jet-parton matching in fully-hadronic $t\bar{t}$ events.

## References
<a id="1">[1]</a>
Lee, J.S.H., Park, I., Watson, I.J. et al, *Zero-permutation jet-parton assignment using a self-attention network*,\
[J. Korean Phys. Soc. 84, 427–438 (2024)](https://doi.org/10.1007/s40042-024-01037-3), arXiv:[2012.03542 [hep-ex]](https://arxiv.org/abs/2012.03542)