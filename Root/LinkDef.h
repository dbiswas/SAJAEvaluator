#include "SAJAEvaluator/SAJAEvaluator.h"

#ifdef __CINT__

#pragma extra_include "SAJAEvaluator/SAJAEvaluator.h";
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#pragma link C++ class SAJAEvaluator+;

#endif
