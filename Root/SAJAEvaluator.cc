#include "SAJAEvaluator/SAJAEvaluator.h"

#include "FastFrames/DefineHelpers.h"
#include "FastFrames/UniqueSampleID.h"
#include <ROOT/RVec.hxx>


void SAJAEvaluator::init() {
  MainFrame::init();

  // Allocate ONNXRuntime session
  m_session = new Ort::Session{Ort::Env{}, "/home/db466094/Documnets/Vts_fit/SAJAEvaluator/model/SAJA_model.onnx", Ort::SessionOptions{nullptr}};  
}

ROOT::RDF::RNode SAJAEvaluator::defineVariables(ROOT::RDF::RNode mainNode,
                                                const UniqueSampleID& /*id*/) {

  // You can also use the UniqueSampleID object to apply a custom defione
  // based on the sample:
  //   id.dsid() returns sample DSID
  //   id.campaign() returns sample campaign
  //   id.simulation() return simulation flavour
  // You can use it in your functions to apply only per sample define

  mainNode = MainFrame::systematicDefine(mainNode,
                                         "SAJA_score_NOSYS", // name of the new column
                                         [this](const ROOT::RVec<ROOT::Math::PtEtaPhiEVector>& jet_TLV, 
                                                const ROOT::RVec<double>& jet_btagScore, 
                                                const ROOT::RVec<float>& jet_ghostTrackPtWeightedCharge, 
                                                const ROOT::RVec<std::vector<float>>& jet_ePerSampling, 
                                                const ROOT::RVec<char>& jet_select_or, 
                                                const ROOT::RVec<ROOT::Math::PtEtaPhiEVector>& el_TLV, 
                                                const ROOT::RVec<char>& el_select_or, 
                                                const ROOT::RVec<ROOT::Math::PtEtaPhiEVector>& mu_TLV, 
                                                const ROOT::RVec<char>& mu_select_or, 
                                                const float& met_met, const float& met_phi, 
                                                const char& pass_emu_tight, const char& pass_ee_tight, const char& pass_mumu_tight) {

                                                  return this->evaluate(jet_TLV[jet_select_or], jet_btagScore[jet_select_or], jet_ghostTrackPtWeightedCharge[jet_select_or], jet_ePerSampling[jet_select_or], el_TLV[el_select_or], mu_TLV[mu_select_or], met_met, met_phi, pass_emu_tight, pass_ee_tight, pass_mumu_tight);
                                                },
                                         {"jet_TLV_NOSYS", "jet_btagScore_DL1dv01_NOSYS", "jet_GhostTrackPtWeightedCharge", "jet_EnergyPerSampling", "jet_select_or_NOSYS", "el_TLV_NOSYS", "el_select_or_NOSYS", "mu_TLV_NOSYS", "mu_select_or_NOSYS", "met_met_NOSYS", "met_phi_NOSYS", "pass_emu_tight_NOSYS", "pass_ee_tight_NOSYS", "pass_mumu_tight_NOSYS"});

  return mainNode;
}

ROOT::RDF::RNode SAJAEvaluator::defineVariablesNtuple(ROOT::RDF::RNode mainNode,
                                                      const UniqueSampleID& /*id*/) {

  return mainNode;
}

ROOT::RDF::RNode SAJAEvaluator::defineVariablesTruth(ROOT::RDF::RNode node,
                                                     const std::string& /*sample*/,
                                                     const UniqueSampleID& /*sampleID*/) {
  return node;
}

float SAJAEvaluator::evaluate(const ROOT::RVec<ROOT::Math::PtEtaPhiEVector>& jet_TLV, 
                              const ROOT::RVec<double>& jet_btagScore, 
                              const ROOT::RVec<float>& jet_ghostTrackPtWeightedCharge, 
                              const ROOT::RVec<std::vector<float>>& jet_ePerSampling, 
                              const ROOT::RVec<ROOT::Math::PtEtaPhiEVector>& el_TLV, 
                              const ROOT::RVec<ROOT::Math::PtEtaPhiEVector>& mu_TLV, 
                              const float& met_met, const float& met_phi, 
                              const char& pass_emu_tight, const char& pass_ee_tight, const char& pass_mumu_tight) {
  
  std::size_t nJets = jet_TLV.size();

  assert(jet_btagScore.size() == nJets && jet_ghostTrackPtWeightedCharge.size() == nJets && jet_ePerSampling.size() == nJets);
  assert(pass_ee_tight + pass_emu_tight + pass_mumu_tight == 1);
  
  if (nJets >= 10) {
    nJets = 10;
  }
  
  std::array<std::array<float,5>,10> jets = {0};
  std::array<int64_t, 3> jets_shape = {1,10,5};
  std::array<std::array<float,28>,10> jets_ePerSampling = {0};
  std::array<int64_t, 3> jets_ePerSampling_shape = {1,10,28};
  std::array<std::array<float,4>,2> leptons = {0};
  std::array<int64_t, 3> leptons_shape = {1,2,4};
  std::array<float,2> met = {0};
  std::array<int64_t, 2> met_shape = {1,2};

  auto memory_info = Ort::MemoryInfo::CreateCpu(OrtDeviceAllocator, OrtMemTypeCPU);

  // std::vector<std::string> jet_vars = {"jet_pt_NOSYS/1000", "jet_eta", "jet_phi", "jet_btagScore_DL1dv01_NOSYS","jet_GhostTrackPtWeightedCharge"};
  // std::vector<std::string> lepton_vars = {"el_pt_NOSYS/1000","el_eta", "el_phi", "el_e_NOSYS/1000", "mu_pt_NOSYS/1000", "mu_eta", "mu_phi", "mu_e_NOSYS/1000"};
  // std::vector<std::string> met_vars = {"met_met_NOSYS/1000", "met_phi_NOSYS"};
  
  const std::array<const char*,4> input_names = {"jets", "jets_ePerSampling", "leptons", "met"};
  const std::array<const char*,1> output_names = {"output"};
  
  for (std::size_t i = 0; i < nJets; i++) {
    jets[i][0] = jet_TLV.at(i).pt()/1000;
    jets[i][1] = jet_TLV.at(i).eta();
    jets[i][2] = jet_TLV.at(i).phi();
    jets[i][3] = static_cast<float>(jet_btagScore.at(i));
    jets[i][4] = jet_ghostTrackPtWeightedCharge.at(i);
    for (std::size_t j=0; j<28; j++) {
      jets_ePerSampling[i][j] = jet_ePerSampling.at(i).at(j)/1000;
    }
  }

  if (pass_emu_tight == 1) {
    if (el_TLV.size() != 1) {
      LOG(WARNING) << "More than one electron is present in this event. Returning -999.0 as the t->s score.\n";
      return -999.0;
    }

    if (mu_TLV.size() != 1) {
      LOG(WARNING) << "More than one muon is present in this event. Returning -999.0 as the t->s score.\n";
      return -999.0;
    }

    leptons[0][0] = el_TLV.at(0).pt()/1000;
    leptons[0][1] = el_TLV.at(0).eta();
    leptons[0][2] = el_TLV.at(0).phi();
    leptons[0][3] = el_TLV.at(0).e()/1000;

    leptons[1][0] = mu_TLV.at(0).pt()/1000;
    leptons[1][1] = mu_TLV.at(0).eta();
    leptons[1][2] = mu_TLV.at(0).phi();
    leptons[1][3] = mu_TLV.at(0).e()/1000;
  }
  else if (pass_ee_tight == 1)
  {
    if (el_TLV.size() > 2) {
      LOG(WARNING) << "More than two electrons are present in this ee event. Returning -999.0 as the t->s score.\n";
      return -999.0;
    }

    if (mu_TLV.size() > 0) {
      LOG(WARNING) << "More than zero muon is present in this ee event. Returning -999.0 as the t->s score.\n";
      return -999.0;
    }

    leptons[0][0] = el_TLV.at(0).pt()/1000;
    leptons[0][1] = el_TLV.at(0).eta();
    leptons[0][2] = el_TLV.at(0).phi();
    leptons[0][3] = el_TLV.at(0).e()/1000;

    leptons[1][0] = el_TLV.at(1).pt()/1000;
    leptons[1][1] = el_TLV.at(1).eta();
    leptons[1][2] = el_TLV.at(1).phi();
    leptons[1][3] = el_TLV.at(1).e()/1000;
  }
  else if (pass_mumu_tight == 1)
  {
    if (el_TLV.size() > 0) {
      LOG(WARNING) << "More than zero electron is present in this mumu event. Returning -999.0 as the t->s score.\n";
      return -999.0;
    }

    if (mu_TLV.size() > 2) {
      LOG(WARNING) << "More than two muons are present in this mumu event. Returning -999.0 as the t->s score.\n";
      return -999.0;
    }

    leptons[0][0] = mu_TLV.at(0).pt()/1000;
    leptons[0][1] = mu_TLV.at(0).eta();
    leptons[0][2] = mu_TLV.at(0).phi();
    leptons[0][3] = mu_TLV.at(0).e()/1000;

    leptons[1][0] = mu_TLV.at(1).pt()/1000;
    leptons[1][1] = mu_TLV.at(1).eta();
    leptons[1][2] = mu_TLV.at(1).phi();
    leptons[1][3] = mu_TLV.at(1).e()/1000;
  }
  

  met[0] = met_met/1000;
  met[1] = met_phi;

  std::vector<Ort::Value> input_tensors;
  input_tensors.push_back(Ort::Value::CreateTensor<float>(memory_info, jets[0].data(), 5*10, jets_shape.data(), jets_shape.size()));
  input_tensors.push_back(Ort::Value::CreateTensor<float>(memory_info, jets_ePerSampling[0].data(), 28*10, jets_ePerSampling_shape.data(), jets_ePerSampling_shape.size()));
  input_tensors.push_back(Ort::Value::CreateTensor<float>(memory_info, leptons[0].data(), 2*4, leptons_shape.data(), leptons_shape.size()));
  input_tensors.push_back(Ort::Value::CreateTensor<float>(memory_info, met.data(), 2, met_shape.data(), met_shape.size()));
  std::vector<Ort::Value> output_tensors = m_session->Run(Ort::RunOptions{nullptr}, input_names.data(), input_tensors.data(), input_tensors.size(), output_names.data(), output_names.size());
  std::array<std::array<float,3>,10> SAJA_output = *output_tensors.at(0).GetTensorData<std::array<std::array<float,3>,10>>();

  // for (std::size_t i=0; i<10; i++) {
  //   for (std::size_t j=0; j<3; j++) {
  //     std::cout << std::fixed << std::setprecision(3) << SAJA_output.at(i).at(j) << " ";
  //   }
  //   std::cout << std::endl;
  // }
  // std::cout << std::endl;
  // std::cout << std::endl;

  float yhat_t_to_s = -999.0;
  for (std::size_t i=0; i<nJets; i++) {
    if (SAJA_output.at(i).at(0) > yhat_t_to_s) {
      yhat_t_to_s = SAJA_output.at(i).at(0);
    }
  }
  // std::cout << yhat_t_to_s << std::endl;
  return yhat_t_to_s;
}